<?php
require "vendor/autoload.php";
use WordPressPluginRepos\PHPHtmlParser\Dom;

class PluginRepos_Discover {

	public function discover($url) {
		$this->http = new WP_HTTP();
		$resp = $this->http->get($url, [
		    'headers' => [
		        'User-Agent' => 'WordPressRepos'
            ]
        ]);
		if(is_wp_error($resp)) throw new Exception("Internal Error: " . $resp->get_error_message());

		if( $resp['response']['code'] != 200 ) {
			throw new Exception("$url returned " . $resp['response']['code'] . " instead of 200: " . esc_html($resp['body']));
		}

		// Is this a webpage?
		$content_type = $resp['headers']['content-type'];
		if(stripos($content_type, 'text/html') !== false) {
			// This is a webpage
			$dom = new Dom;
			$dom->load($resp['body']);
			$link = $dom->find('link');
			foreach ($link as $link) {
				if( $link->getAttribute('rel') == 'wp-plugin-repo' ) {
					$url = $link->getAttribute('href');
					return $this->discover($url);
				}
			}
		} else {
			$j = json_decode($resp['body'], true);
			if(!$j) {
				throw new Exception("Could not understand Repo" . $resp['body']);
			}
			if(!$j['repo']['url']){
				$j['repo']['url'] = $url;
			}
			return $j;
		}
	}

}