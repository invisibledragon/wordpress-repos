<?php
/**
 * @package Plugin Repos
 * @version 1.3.0
 */
/*
Plugin Name: Plugin Repos
Plugin URI: https://invisibledragonltd.com/wordpress/repo/
Description: Add-on to allow WordPress to install addons from external repositories
Author: Invisible Dragon
Author URI: https://invisibledragonltd.com/
License: TBC
Version: 1.3.0
*/
define('PLUGIN_REPOS_VERSION', '1.3.0');

add_action( 'plugins_loaded', array( 'PluginReposPlugin', 'get_instance' ) );


class PluginReposPlugin {
	protected static $instance = null;

	public $plugin_name = 'plugin-repos';

	public static function get_instance() {
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	private function __construct(){
		add_action( 'admin_menu', array( $this, 'admin_menu' ) );

		add_filter( 'views_plugin-install', array( $this, 'plugin_install_view_menu' ) );

		add_filter( 'plugins_api', array( $this, 'w_org_api' ), 10, 3 );
        add_filter( 'themes_api', array( $this, 'w_org_api' ), 10, 3 );

        add_filter( 'pre_set_site_transient_update_plugins', array( $this, 'update_plugins' ) );
        add_filter( 'pre_set_site_transient_update_themes', array( $this, 'update_themes' ) );
	}

	public function update_repo($repo_id) {
		$repos = $this->get_repo_list();
		$repo = $repos[$repo_id];

		$http = new WP_HTTP();
		$args = array();
		if($repo['auth_token']) {
			$args['headers'] = [
				'Authorization' => 'Bearer ' . $repo['auth_token']
			];
		}
		$resp = $http->get($repo['url'], $args);
		if(is_wp_error($resp)) return $resp;

		try{
			$body = json_decode($resp['body'], true);
			if(!$body) throw new Exception('JSON failed');

			$repo['refreshed'] = time();
			if($body['repo']['url']) {
				$repo['url'] = $body['repo']['url'];
			}
			if($body['repo']['name']) {
				$repo['name'] = $body['repo']['name'];
			}
			if($body['repo']['auth']['token']) {
				$repo['auth_token'] = $body['repo']['auth']['token'];
			}
			$repos[$repo_id] = $repo;
			$this->set_repo_list($repos);

			if(!empty($body['plugins'])) {
                $cached = array();
                foreach ($body['plugins'] as $plugin) {
                    $plugin['repo_name'] = $body['repo']['name'];
                    update_option('plugin-repos-' . esc_attr($plugin['slug']), $plugin, false);
                    $cached[] = $plugin['slug'];
                }
                update_option('plugin-repos-cached-' . $repo_id, $cached, false);
            }

			return $body;
		} catch (Exception $e) {
			return array('error' => $resp['body']);
		}
	}

    public function update_themes($transient) {

        if ( ! is_object( $transient ) )
            $transient = new stdClass;

        if ( ! isset( $transient->response ) || ! is_array( $transient->response ) )
            $transient->response = array();

        $repos = $this->get_repo_list();
        $installed = wp_get_themes();

        foreach ($repos as $id => $value) {
            $this->update_repo($id);

            $plugins = get_option('plugin-repos-cached-' . $id);

            if($plugins) {
                foreach($plugins as $plugin) {

                    $data = get_option('plugin-repos-' . $plugin);
                    if($data['type'] != 'theme') continue;

                    // Try to find our plugin
                    foreach($installed as $key => $installed_details) {
                        if($key == $plugin) {
                            $installed_version = $installed_details->Version;
                            if(version_compare( $installed_version, $data['version'], '<' ) && version_compare($data['requires'], get_bloginfo('version'), '<' )) {

                                // We have an update!
                                $transient->response[$key] = array(
                                    'slug' => $data['slug'],
                                    'plugin' => $key,
                                    'new_version' => $data['version'],
                                    'tested' => $data['tested'],
                                    'package' => $data['download_link'],
                                    'icons' => $data['icons'],
                                    'upgrade_notice' => sprintf(__('This update is provided by %s'), $data['repo_name'])
                                );

                            }
                        }
                    }

                }
            }
        }

        return $transient;

    }

	public function update_plugins($transient) {

        if ( ! is_object( $transient ) )
			$transient = new stdClass;

		if ( ! isset( $transient->response ) || ! is_array( $transient->response ) )
			$transient->response = array();

		$repos = $this->get_repo_list();
		$installed = get_plugins();

		foreach ($repos as $id => $value) {
	        $this->update_repo($id);

	        $plugins = get_option('plugin-repos-cached-' . $id);


	        if($plugins) {
	        	foreach($plugins as $plugin) {

	        		$data = get_option('plugin-repos-' . $plugin);
                    if($data['type'] == 'theme') continue;

                    // Try to find our plugin
	        		foreach($installed as $key => $installed_details) {
	        			if(explode("/", $key)[0] == $plugin) {
	        				$installed_version = $installed_details['Version'];
	        				if(version_compare( $installed_version, $data['version'], '<' ) && version_compare($data['requires'], get_bloginfo('version'), '<' )) {

	        					// We have an update!
	        					$transient->response[$key] = (object)array(
	        						'slug' => $data['slug'],
	        						'plugin' => $key,
	        						'new_version' => $data['version'],
	        						'tested' => $data['tested'],
	        						'package' => $data['download_link'],
	        						'icons' => $data['icons'],
	        						'upgrade_notice' => sprintf(__('This update is provided by %s'), $data['repo_name'])
	        					);

	        				}
	        			}
	        		}

	        	}
	        }
	    }

		return $transient;

	}

	public function w_org_api($result, $action, $args) {
		if( $action == 'plugin_information' || $action == 'theme_information' ) {
			
			$slug = $args->slug;
			// If this returns false (i.e none) then WordPress will call the W.org api
			$result = get_option('plugin-repos-' . $slug);

			if($result['banner_text_right']) {
				add_action('admin_head', function() {
					echo '<style type="text/css">#plugin-information-title{ text-align: right; }</style>';
				});
			}

			if($result) {
				$result = (object)$result;
			}

		}
		return $result;
	}

	public function get_repo_list() {
		$options = get_option('plugin-repos');
		if(!$options) $options = [
			// By default we add ourselves
			'selfupdates' => [
				'url' => 'https://invisibledragon.gitlab.io/wordpress-repos/repo.json',
				'refreshed' => 0,
				'name' => 'WordPress Plugin Repos Updates'
			]
		];
		return $options;
	}

	public function set_repo_list($repos) {
		update_option('plugin-repos', $repos);
	}

	public function plugin_install_view_menu($views) {

		$options = '<option value="' . admin_url('plugin-install.php') . '">Wordpress.org</option>';

		$repos = $this->get_repo_list();
		foreach ($repos as $id => $value) {
			$s = '';
			if($_GET['repo'] == $id) $s = 'selected';
			$options .= '<option ' . $s . ' value="' . admin_url('plugins.php?page=plugin-repos&repo=' . $id) . '">' . esc_attr($value['name']) . '</option>';
		}

		$options .= '<option value="' . admin_url('options-general.php?page=plugin-repos') . '">' . __('Configure Repos', $this->plugin_name) . '</option>';

		wp_enqueue_script( 'plugin-repoos', plugins_url( 'js/admin.js', __FILE__ ), array('jquery'), PLUGIN_REPOS_VERSION );

		return array_merge(array(
			'plugin-install-source' => '<select class="plugin_repos_select_repo">' . $options . '</select>'
		), $views);
	}

	public function repo_browse() {
		global $wp_list_table;

		if ( ! current_user_can( 'install_plugins' ) ) {
			wp_die( __( 'Sorry, you are not allowed to install plugins on this site.' ) );
		}

		// Add the Wordpress script to make it the same UX as normal install
		add_thickbox();
		wp_enqueue_script( 'plugin-install' );

		$repos = $this->get_repo_list();
		if(!$_GET['repo']) $_GET['repo'] = array_keys($repos)[0];
		$repo = $repos[$_GET['repo']];
		$repo['id'] = $_GET['repo'];

		?>
		<div class="wrap">
			<style type="text/css">
				/* not something we're overly bothered about in repos */
				.column-rating, .column-downloaded { display: none; }
			</style>
			<h1 class="wp-heading-inline">
				<?= __('Add New from Repo', $this->plugin_name ); ?>
			</h1>
			<?php
				require_once plugin_dir_path( __FILE__ ) . 'list-table.php';
				$wp_list_table = new Plugin_Repo_List_Table($repo);

				$wp_list_table->prepare_items();

				$wp_list_table->views();

				display_plugins_table();
			?>
		</div>
		<?php

		// Update repo name if needed
		if($repo['name'] != $wp_list_table->repo['repo']['name'] && !empty($wp_list_table->repo['repo']['name'])) {
			$repos[$_GET['repo']]['name'] = $wp_list_table->repo['repo']['name'];
			$this->set_repo_list($repos);
		}
	}

	public function admin_menu() {
		add_plugins_page(
			__( 'Add New from Repo', $this->plugin_name ),
			__( 'Add New from Repo', $this->plugin_name ),
			'upload_plugins',
			$this->plugin_name,
			array( $this, 'repo_browse' ),
			2
		);

		add_options_page(
			__( 'Plugin Repos', $this->plugin_name ),
			__( 'Plugin Repos', $this->plugin_name ),
			'manage_options',
			$this->plugin_name,
			array( $this, 'settings_page' )
		);
	}

	public function settings_page() {
		new PluginRepos_Admin($this);	
	}

}


class PluginRepos_Admin {
	public function __construct($plugin) {
		$this->plugin = $plugin;

		?>
		<div class="wrap">
			<h1><?= __('Plugin Repos', $this->plugin->plugin_name); ?></h1>
			<?php
				$action = 'action_' . $_GET['action'];
				if(!$_GET['action']) {
					$action = 'action_list';
				}
				$this->$action();
			?>
		</div>
		<?php
	}

	public function redirect($url) {
		header("Location: " . $url);
		?>
		<form method="get" action="<?= esc_attr($url); ?>">
			<?= submit_button(__('Click if you are not redirected')); ?>
		</form>
		<?php
	}

	public function action_apply_auth() {

		// Callback
		$repos = $this->plugin->get_repo_list();
		
		$repos[$_GET['repo']]['auth_token'] = $_GET['token'];

		$this->plugin->set_repo_list($repos);

		$this->redirect(admin_url('plugins.php?page=plugin-repos&repo=' . urlencode($_GET['repo'])));

	}

	public function action_auth() {

		$repos = $this->plugin->get_repo_list();
		$repo = $repos[$_GET['repo']];

		$http = new WP_HTTP();
		$resp = $http->get($repo['url']);
		$body = json_decode($resp['body'], true);

		if($body['repo']['auth'] && $body['repo']['auth']['available']) {
			$url = $body['repo']['auth']['url'] . '?return_url=' . urlencode(home_url(add_query_arg('action', 'apply_auth')));
			$this->redirect($url);
		}

	}

	public function action_list() {
		$repos = $this->plugin->get_repo_list();

		if($repos): ?>
			<table class="widefat" style="width: auto">
				<?php foreach($repos as $id => $repo): ?>
					<tr>
						<td>
							<strong><?= $repo['name']; ?></strong><br/>
							<?= $repo['url']; ?><?php if($_GET['debug']): ?><br/>
							<?= $repo['auth_token']; ?>
							<?php endif; ?>
						</td>
						<td>
							<a href="<?= add_query_arg(array('action' => 'remove', 'id' => $id)); ?>">
								<?= __('Remove', 'wprepo'); ?>
							</a>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		<?php else: ?>
			<p><?= __('There are no repos added to this WordPress installation', $this->plugin->plugin_name); ?></p>
		<?php endif; ?>
		<style>form p.submit{ display: inline-block; }</style>
		<form method="post" action="<?= add_query_arg('action', 'add'); ?>">
			<?= __('Add a new repo:', $this->plugin->plugin_name); ?>
			<input type="text" name="url" placeholder="<?= __('URL'); ?>" />
			<?= submit_button(__('Add')); ?>
		</form>
		<?php
	}

	public function action_add() {
		require_once plugin_dir_path(__FILE__) . "repo-discover.php";

		$discover = new PluginRepos_Discover();
		try{
			$url = $_POST['url'];
			$resp = $discover->discover($_POST['url']);

			if($resp['repo']['url']) {
				$url = $resp['repo']['url'];
			}

			?>
			<p><?= __("Do you wish to add this repo?"); ?></p>
			<table class="widefat">
				<tr>
					<th><?= __('Name of Repo'); ?></th>
					<td><?= esc_html($resp['repo']['name']); ?></td>
				</tr>
				<tr>
					<th><?= __('Available plugins'); ?></th>
					<td>
						<ul>
							<?php foreach($resp['plugins'] as $plugin): ?>
								<li><?= esc_html($plugin['name']); ?></li>
							<?php endforeach; ?>
						</ul>
					</td>
				</tr>
			</table>
			<form method="post" action="<?= add_query_arg('action', 'commit_add'); ?>">
				<input type="hidden" name="url" value="<?= esc_attr($url); ?>" />
				<input type="hidden" name="name" value="<?= esc_attr($resp['repo']['name']); ?>" />
				<input type="hidden" name="auth_token" value="<?= esc_attr($resp['repo']['auth']['token']); ?>" />
				<?= submit_button(_('Add')); ?>
			</form>
			<?php
		} catch(Exception $e) {
			?>
			<div class="notice notice-error inline">
				<p><strong><?= __("Could not load a valid repo from that address"); ?></strong></p>
				<p><?= $e; ?></p>
			</div>
			<?php
		}
	}

	public function action_remove() {
		?>
		<div class="notice notice-warning inline">
			<p><strong><?= __("Are you sure you want to delete this repo?"); ?></strong></p>
			<p>
				<a href="<?= add_query_arg(array('action' => 'commit_remove', 'id' => $_GET['id'])); ?>">
					<?= __('Remove'); ?>
				</a>
			</p>
		</div>
		<p>
			<a href="<?= add_query_arg(array('action' => 'list')); ?>">
				<?= __('Return to list'); ?>
			</a>
		</p>
		<?php
	}

	public function action_commit_remove() {
		$repos = $this->plugin->get_repo_list();
		unset($repos[$_GET['id']]);
		$this->plugin->set_repo_list($repos);

		$this->redir(add_query_arg(array('action' => 'list')));
	}

	public function action_commit_add() {
		$repos = $this->plugin->get_repo_list();
		$id = time();
		$repos[$id] = ['url' => $_POST['url'], 'name' => $_POST['name'], 'auth_token' => $_POST['auth_token']];
		$this->plugin->set_repo_list($repos);

		$this->redir(admin_url('plugins.php?page=plugin-repos&repo=' . $id));
		
	}

	public function redir($url) {
		?>
		<a class="redir" href="<?= $url; ?>">If you are not redirected</a>
		<script>
			(function($){
				document.location.href = $(".redir").attr("href");
                $(".redir").text("You are being redirected. One moment...");
			})(jQuery);
		</script>
		<?php
	}

}
