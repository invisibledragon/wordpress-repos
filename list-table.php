<?php

require_once(ABSPATH . 'wp-admin/includes/class-wp-plugin-install-list-table.php');

class Plugin_Repo_List_Table extends WP_Plugin_Install_List_Table {

	public $repo_url = '';
	public $repo_id = -1;
	public $repo_info = [];
	public $repo = [];

	public function __construct($repo) {
		parent::__construct();
		$this->repo_id = $repo['id'];
		$this->repo_url = $repo['url'];
		$this->repo_info = $repo;
	}

	/**
	 * @global array $tabs
	 * @global string $tab
	 *
	 * @return array
	 */
	protected function get_views() {
		global $tabs, $tab;
		$display_tabs = array();
		foreach ( (array) $tabs as $action => $text ) {
			$current_link_attributes                     = ( $action === $tab ) ? ' class="current" aria-current="page"' : '';
			$href                                        = self_admin_url( 'plugins.php?page=plugin-repos&tab=' . $action );
			$display_tabs[ 'plugin-install-' . $action ] = "<a href='$href'$current_link_attributes>$text</a>";
		}
		// No longer a real tab.
		unset( $display_tabs['plugin-install-upload'] );
		return $display_tabs;
	}


	/**
	 * Override parent views so we can use the filter bar display.
	 */
	public function views() {
		$views = $this->get_views();
		/** This filter is documented in wp-admin/inclues/class-wp-list-table.php */
		$views = apply_filters( "views_plugin-install", $views );
		$this->screen->render_screen_reader_content( 'heading_views' );
		?>
<div class="wp-filter">
	<ul class="filter-links">
		<?php
		if ( ! empty( $views ) ) {
			foreach ( $views as $class => $view ) {
				$views[ $class ] = "\t<li class='$class'>$view";
			}
			echo implode( " </li>\n", $views ) . "</li>\n";
		}
		?>
		<?php if($this->repo['repo']['auth']['available'] && $this->repo['repo']['auth']['authenticated'] && $this->repo['repo']['auth']['manage_url']): ?>
		<li>
			<a href="<?= $this->repo['repo']['auth']['manage_url'] . '&return_url=' . urlencode(home_url(add_query_arg())); ?>">
				<?= __('Manage'); ?>
			</a>
		</li>
		<?php endif; ?>
	</ul>
	<?php /* do not call search filter */ ?>
</div>
		<?php

		// Now ask for auth if not given
		if($this->repo['repo']['auth']['available'] && !$this->repo['repo']['auth']['authenticated']):
			?>
			<div class="notice notice-info inline">
				<p>
					<?= __('This repository supports authentication. You can authenticate this installation of WordPress to this repository.'); ?>
				</p>
				<form method="post" action="<?= admin_url('options-general.php?page=plugin-repos&action=auth&repo=' . $this->repo_id); ?>">
					<?= submit_button(__('Authenticate')); ?>
				</form>
			</div>
			<?php
		endif;
	}

    /**
     * From https://github.com/WordPress/WordPress/blob/9b44c28f786d0321bb7f632ca9271ca75a5f07fe/wp-admin/includes/class-wp-theme-install-list-table.php#L544
     *
     * Changed objects to arrays
     */
    private function _get_theme_status( $theme ) {
        $status = 'install';

        $installed_theme = wp_get_theme( $theme['slug'] );
        if ( $installed_theme->exists() ) {
            if ( version_compare( $installed_theme->get( 'Version' ), $theme['version'], '=' ) ) {
                $status = 'latest_installed';
            } elseif ( version_compare( $installed_theme->get( 'Version' ), $theme['version'], '>' ) ) {
                $status = 'newer_installed';
            } else {
                $status = 'update_available';
            }
        }

        return $status;
    }

	public function install_action_links( $links, $plugin ) {

		if($plugin['buy_link'] && !$plugin['download_link']) {
			$links[0] = '<a target="_blank" class="button" href="' . esc_attr($plugin['buy_link']) . '">' . __('Buy Plugin') . '</a>';
		} elseif($plugin['type'] == 'theme') {
            $status = $this->_get_theme_status( $plugin );

            switch ($status) {
                case 'update_available':
                    $url = add_query_arg(
                        array(
                            'action' => 'upgrade-theme',
                            'theme'  => $plugin['slug'],
                        ),
                        self_admin_url( 'update.php' )
                    );
                    $url = wp_nonce_url( $url, 'upgrade-theme_' . $plugin['slug'] );
                    $links[0] = '<a class="button" data-name="' . esc_attr($plugin['name']) . '" href="' . esc_attr($url) . '">' . __('Update Theme') . '</a>';
                    break;
                case 'newer_installed':
                case 'latest_installed':
                    $links[0] = sprintf(
                        '<span class="install-now" title="%s">%s</span>',
                        esc_attr__( 'This theme is already installed and is up to date' ),
                        _x( 'Installed', 'theme' )
                    );
                    break;
                case 'install':
                default:
                    $url = add_query_arg(
                        array(
                            'action' => 'install-theme',
                            'theme'  => $plugin['slug'],
                        ),
                        self_admin_url( 'update.php' )
                    );
                    $url = wp_nonce_url( $url, 'install-theme_' . $plugin['slug'] );
                    $links[0] = '<a class="button" data-name="' . esc_attr($plugin['name']) . '" href="' . esc_attr($url) . '">' . __('Install Theme') . '</a>';
            }

            // TODO: Support popup better
            unset($links[1]);

        }

		if($plugin['plugin_licensed']) {
			$links[] = '<span class="dashicons dashicons-admin-network"></span> ' . __('Plugin Licensed');
		}

		return $links;

	}


	/**
	 * @global array  $tabs
	 * @global string $tab
	 * @global int    $paged
	 * @global string $type
	 * @global string $term
	 */
	public function prepare_items() {
		include_once( ABSPATH . 'wp-admin/includes/plugin-install.php' );
		global $tabs, $tab, $paged, $type, $term;
		wp_reset_vars( array( 'tab' ) );
		$paged = $this->get_pagenum();
		$per_page = 36;
		// These are the tabs which are shown on the page
		$tabs = array();

		$tabs['all']    = __( 'All' );
		$nonmenu_tabs = array( 'plugin-information' ); // Valid actions to perform which do not have a Menu item.

		// If a non-valid menu tab has been selected, And it's not a non-menu action.
		if ( empty( $tab ) || ( ! isset( $tabs[ $tab ] ) && ! in_array( $tab, (array) $nonmenu_tabs ) ) ) {
			$tab = key( $tabs );
		}
		$installed_plugins = $this->get_installed_plugins();

		$this->repo = PluginReposPlugin::get_instance()->update_repo($this->repo_id);
		if(is_wp_error($this->repo)) {
		    $this->repo = [
                'error' => $this->repo
            ];
        }

        add_filter( 'plugin_install_action_links', array( $this, 'install_action_links' ), 10, 2 );

		if(array_key_exists('plugins', $this->repo['plugins'])) {

			?>
			<div class="notice notice-info inline">
				<p><strong>There was a problem talking to this repo</strong></p>
				<p>Technical details:</p>
				<pre><?= esc_html($this->repo['error']); ?></pre>
			</div>
			<?php

			return;

		}

		$this->items = $this->repo['plugins'];

		if ( $this->orderby ) {
			uasort( $this->items, array( $this, 'order_callback' ) );
		}
		$this->set_pagination_args(
			array(
				'total_items' => $api->info['results'],
				'per_page'    => $args['per_page'],
			)
		);
		if ( isset( $api->info['groups'] ) ) {
			$this->groups = $api->info['groups'];
		}
		if ( $installed_plugins ) {
			$js_plugins = array_fill_keys(
				array( 'all', 'search', 'active', 'inactive', 'recently_activated', 'mustuse', 'dropins' ),
				array()
			);
			$js_plugins['all'] = array_values( wp_list_pluck( $installed_plugins, 'plugin' ) );
			$upgrade_plugins   = wp_filter_object_list( $installed_plugins, array( 'upgrade' => true ), 'and', 'plugin' );
			if ( $upgrade_plugins ) {
				$js_plugins['upgrade'] = array_values( $upgrade_plugins );
			}
			wp_localize_script(
				'updates',
				'_wpUpdatesItemCounts',
				array(
					'plugins' => $js_plugins,
					'totals'  => wp_get_update_data(),
				)
			);
		}
	}


}